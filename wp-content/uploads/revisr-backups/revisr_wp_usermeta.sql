DROP TABLE IF EXISTS `wp_usermeta`;
CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
LOCK TABLES `wp_usermeta` WRITE;
INSERT INTO `wp_usermeta` VALUES ('1','1','nickname','admin'), ('2','1','first_name',''), ('3','1','last_name',''), ('4','1','description',''), ('5','1','rich_editing','true'), ('6','1','comment_shortcuts','false'), ('7','1','admin_color','fresh'), ('8','1','use_ssl','0'), ('9','1','show_admin_bar_front','true'), ('10','1','wp_capabilities','a:1:{s:13:\"administrator\";b:1;}'), ('11','1','wp_user_level','10'), ('12','1','dismissed_wp_pointers','wp350_media,wp360_revisions,wp360_locks,wp390_widgets'), ('13','1','show_welcome_panel','1'), ('14','1','session_tokens','a:3:{s:64:\"555c7c544d2bb2de14e06dd2b089e7dc3ac590422495315547dc94e31fe32f06\";i:1436522039;s:64:\"347a446d51112448fb4dbe2521e0548dd307b41649c2c53e086bc61be073b987\";i:1436523361;s:64:\"7bbb0242a941060582fab62b0251f709e4d513c17d05b8b7ad2928c501e8268a\";i:1436523600;}'), ('15','1','wp_dashboard_quick_press_last_post_id','3');
UNLOCK TABLES;
